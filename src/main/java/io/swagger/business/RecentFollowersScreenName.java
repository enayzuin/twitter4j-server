package io.swagger.business;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import io.swagger.model.Users;
import io.swagger.utils.FileUtils;
import twitter4j.Twitter;
import twitter4j.User;

@Service
public class RecentFollowersScreenName {

	private static final String FILE_PATH = "src\\main\\resources\\recentFollower";

	public static ArrayList<io.swagger.model.User> getScreenName() throws IOException {
		String kinJutsu = FileUtils.fileToString(FILE_PATH);

		ArrayList<io.swagger.model.User> listUser = new ArrayList<io.swagger.model.User>();
		Twitter twitter = TwitterCredentials.getCredentials();

		List<String> followers = GetFollowersId.list(twitter, kinJutsu);

		for (String follower : followers) {

			try {
				User user = twitter.showUser(Long.parseLong(follower));
				io.swagger.model.User apiUser = new io.swagger.model.User();
				apiUser.setScreenName(user.getScreenName());
				listUser.add(apiUser);
			} catch (Exception e) {
				break;
			}
		}
		try {
			FileUtils.stringToFile(FILE_PATH, listUser.get(0).getScreenName());
		} catch (Exception e) {
		}
		return listUser;

	}

}
