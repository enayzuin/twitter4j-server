package io.swagger.business;

import java.util.ArrayList;
import java.util.List;

import twitter4j.IDs;
import twitter4j.Twitter;
import twitter4j.TwitterException;

public class GetFollowersId {

	public static List<String> list(Twitter twitter, String kinJutsu) {
		List<String> listIds = new ArrayList<String>();
		int cont = 0;
		try {
			long cursor = -1;
			IDs ids;
			System.out.println("Listing followers's ids.");
			do {
				ids = twitter.getFollowersIDs(cursor);
				for (long id : ids.getIDs()) {
					System.out.println(id);

					if (twitter.showUser(id).getScreenName().equals(kinJutsu) || cont == 10) {
						return listIds;
					}
					listIds.add(String.valueOf(id));
					cont++;
				}
			} while ((cursor = ids.getNextCursor()) != 0);
		} catch (TwitterException te) {
			te.printStackTrace();
			System.out.println("Failed to get followers' ids: " + te.getMessage());
			return listIds;
		}
		return listIds;
	}

}
