
package io.swagger.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.fasterxml.jackson.databind.ObjectMapper;

import io.swagger.business.RecentFollowersScreenName;
import io.swagger.model.User;
import io.swagger.model.Users;
import io.swagger.utils.FileUtils;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-02-17T04:43:10.588Z")

@Controller
public class UsersApiController implements UsersApi {

	private static final Logger log = LoggerFactory.getLogger(UsersApiController.class);

	private final ObjectMapper objectMapper;

	private final HttpServletRequest request;

	@org.springframework.beans.factory.annotation.Autowired
	public UsersApiController(ObjectMapper objectMapper, HttpServletRequest request) {
		this.objectMapper = objectMapper;
		this.request = request;
	}

	public ResponseEntity<ArrayList<User>> usersGet() throws IOException {

		ArrayList<User> users = RecentFollowersScreenName.getScreenName();
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
		headers.add("Access-Control-Allow-Origin", "*");

		return new ResponseEntity<ArrayList<User>>(users, headers, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Void> usersOptions() {
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
		headers.add("Access-Control-Allow-Origin", "*");
		headers.add("Authorization", "Bearer 111111");
		headers.add("Content-Type", "application/json; charset=utf-8");
		headers.add("Accept", "application/json");
		headers.add("Access-Control-Allow-Methods", "GET, POST, PUT, OPTIONS, DETELE");
		headers.add("'Access-Control-Allow-Headers","*");

		return new ResponseEntity<Void>(headers, HttpStatus.OK);
	}


}
