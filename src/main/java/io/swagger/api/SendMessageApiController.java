package io.swagger.api;

import io.swagger.model.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.*;
import io.swagger.business.RecentFollowersScreenName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import javax.validation.Valid;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-02-17T21:13:03.244Z")

@Controller
public class SendMessageApiController implements SendMessageApi {

	private static final Logger log = LoggerFactory.getLogger(SendMessageApiController.class);

	private final ObjectMapper objectMapper;

	private final HttpServletRequest request;

	@org.springframework.beans.factory.annotation.Autowired
	public SendMessageApiController(ObjectMapper objectMapper, HttpServletRequest request) {
		this.objectMapper = objectMapper;
		this.request = request;
	}

	public ResponseEntity<User> sendMessagePost(
			@ApiParam(value = "Corpo da requisição de envio de mensagem", required = true) @Valid @RequestBody User body) {

		System.out.println(body.getScreenName());
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
		headers.add("Access-Control-Allow-Origin", "*");
		headers.add("Authorization", "Bearer 111111");
		headers.add("Content-Type", "application/json; charset=utf-8");
		headers.add("Accept", "application/json");
		headers.add("Access-Control-Allow-Methods", "GET, POST, PUT, OPTIONS, DETELE");
		headers.add("'Access-Control-Allow-Headers","*");

		User user = new User();
		user.setScreenName(body.getScreenName());
		return new ResponseEntity<User>(user, headers, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<Void> sendMessageOptions() {
		MultiValueMap<String, String> headers = new LinkedMultiValueMap<String, String>();
		headers.add("Access-Control-Allow-Origin", "*");
		headers.add("Authorization", "Bearer 111111");
		headers.add("Content-Type", "application/json; charset=utf-8");
		headers.add("Accept", "application/json");
		headers.add("Access-Control-Allow-Methods", "GET, POST, PUT, OPTIONS, DETELE");
		headers.add("'Access-Control-Allow-Headers","*");

		return new ResponseEntity<Void>(headers, HttpStatus.OK);
	}

}
