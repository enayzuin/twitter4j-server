package io.swagger.utils;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileUtils {
	public static String fileToString(String path) throws IOException {

		byte[] encoded = Files.readAllBytes(Paths.get(path));
		String string = new String(encoded, "utf-8");
		return string;

	}

	public static void stringToFile(String path,String text) throws IOException {

		try (PrintWriter out = new PrintWriter(path)) {
			out.print(text);
		}

	}

}
